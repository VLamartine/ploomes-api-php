<?php

namespace Ploomes;

use GuzzleHttp\Client as GuzzleClient;
use Exception;

class PloomesApi {

	public static $CONTACT_PERSON 	= 2;
	public static $CONTACT_COMPANY 	= 1;
	public static $BRAZIL_ID		= 76;

	private $userKey = null;

	private $baseUrl = "https://api2.ploomes.com/";

	private $client = null;

	private $dealName = "Consultoria Mautic";
	/**
	* Construcor
	* @param string $userKey
	*/
	public function __construct($userKey = ""){
		if(empty($userKey))
			throw new Exception("User key must not be empty");

		$this->userKey = $userKey;
		$this->client = new GuzzleClient(['base_uri' => $this->baseUrl]);
	}

	/**
	* Make the request
	* @param string $endpoint Endpoint da api
    * @param string $method The HTTP request verb
    * @param array $fields the post/patch fields
    * @param array $filters filters to be used in GET methods
	* 
	* @return array of stdClass objects 
	*/
	public function request($endPoint, $method = "GET", $fields =[], $filters = ""){
		$url = $endPoint;

		if(!empty($filters))
			$url .= $filters;

		$options = [
			'headers' => ['User-Key' => $this->userKey]
		];

		if(!empty($fields)){
			$options['json'] = $fields;
		}

		try {
			$response = $this->client->request($method, $url, $options);
			$response = $response->getBody()->getContents();
			$response = json_decode($response);
			return $response->value;
		} catch (Exception $e){
			return ["errors" => ["error" => $e->getMessage(), "code" => $e->getCode()]];
		}
	}

	/**
	* Add interaction to ploomes
	*
	* @param array $data - Dados do contato a ser salvo
	* @throws \Exception
	* @return stdClass
	*/
	public function createContact($data){
		$contact = $this->getContactByEmail($data["Email"]);
		if(isset($data['Phones']))
			$data['Phones'] = $this->formatPhone($data['Phones']);
		if(!empty($contact)){
			throw new Exception("O email já está cadastrado");
		} else {
			$newContact = $this->request("Contacts", "POST", $data);
			if(isset($newContact['errors']))
				throw new Exception("Erro ao salvar contato.<br>Código: ".$newContact['errors']['code']."<br>Erro: ".$newContact['errors']['error']);
			return $newContact;
		}
	}

	/**
	* Add interaction to ploomes
	*
	* @param array $data - Dados do contato a ser salvo
	* @throws \Exception
	* @return stdClass
	*/
	public function updateContact($data){
		$contacts = $this->getContactByEmail($data["Email"]);
		
		if (empty($contacts) || count($contacts) == 0) {
			$contact = $this->createContact($data);
		} else {
			$contact = $contacts[0];
		}
		
		if(isset($data['Phones'])) {
			$data['Phones'] = $this->formatPhone($data['Phones']);
		}
		
		$filter = "(".$contact->Id.")";
		
		$updatedContact = $this->request("Contacts", "PATCH", $data, $filter);
		
		if(isset($updatedContact['errors'])) {
			throw new Exception("Erro ao atualizar contato.<br>Código: ".$updatedContact['errors']['code']."<br>Erro: ".$updatedContact['errors']['error']);
		}
		
		return $updatedContact[0];
	}

	/**
	* Add interaction to ploomes
	*
	* @param int $contactId
	* @throws \Exception
	* @return stdClass
	*/
	public function createDeal($contactId){
		if(empty($contactId))
			throw new Exception("O id do contato e obrigatorio");

		$dealParams = [
			"Title" => $this->dealName,
			"ContactId" => $contactId
		];

		$deal = $this->request("Deals", "POST", $dealParams);
		if(isset($deal['errors']))
			throw new Exception("Erro ao salvar deal.<br>Código: ".$deal['errors']['code']."<br>Erro: ".$deal['errors']['error']);
		return $deal[0];
	}

	/**
	* Add interaction to ploomes
	*
	* @param int $contactId
	* @param int $dealId
	* @param string $message
	* @throws \Exception
	* @return stdClass
	*/
	public function addInteraction($contactId, $dealId = "", $message){
		if(empty($contactId))
			throw new Exception("O id do contato deve ser informado");

		$interaction = [
			"ContactId" => $contactId,
			"DealId" => empty($dealId) ? null : $dealId,
			"Content" => $message
		];

		$interaction = $this->request("InteractionRecords", "POST", $interaction);
		if(isset($interaction['errors']))
			throw new Exception("Erro ao salvar interacao.<br>Código: ".$interaction['errors']['code']."<br>Erro: ".$interaction['errors']['error']);
		return $interaction[0];
			
	}
	/**
	*	Convert phone to ploomes format 
	*
	*	@param string $phone
	*	@return array $phones
	*/

	private function formatPhone($phone){
		return [
			[
				"PhoneNumber" => $phone,
				"TypeId" => 2,
				"CountryId" => self::$BRAZIL_ID
			]
		];
	}
	/**
	*	Search contact by email
	*
	*	@param string $email
	*	@return array $contacts 
	*/
	public function getContactByEmail($email){
		$filter = "?\$filter=Email+eq+'".$email."'";
		return $this->request("Contacts", "GET", [], $filter);
	}
}